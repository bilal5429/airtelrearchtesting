/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aricent.vvc

import CharacterTestAdapter
import CharacterModel
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var characterTestAdapter: CharacterTestAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        // Switch to AppTheme for displaying the activity
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Your code
        characterTestAdapter = CharacterTestAdapter()
        character_recyclerview.adapter = characterTestAdapter

        val potterApi = Retrofit.Builder()
                .baseUrl((application as TestUrlApplication).getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(OkHttpProvider.getOkHttpClient())
                .build()
                .create(PotterApi::class.java)

        potterApi.getCharacters().enqueue(object : Callback<List<CharacterModel>> {
            override fun onFailure(call: Call<List<CharacterModel>>, t: Throwable) {
                showErrorState()
            }

            override fun onResponse(call: Call<List<CharacterModel>>,
                                    response: Response<List<CharacterModel>>) {
                if (response.isSuccessful && response.body() != null) {
                    val characterList = response.body()!!
                    if (characterList.isEmpty()) {
                        showEmptyDataState()
                    } else {
                        showCharacterList(characterList)
                    }
                } else {
                    showErrorState()
                }
            }
        })
    }

    private fun showEmptyDataState() {
        character_recyclerview.visibility = View.GONE
        progress_bar.visibility = View.GONE
        textview.visibility = View.VISIBLE
        textview.text = "No Data"
    }

    private fun showCharacterList(characterList: List<CharacterModel>) {
        character_recyclerview.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
        textview.visibility = View.GONE
        characterTestAdapter.setCharacterList(characterList)
    }

    private fun showErrorState() {
        character_recyclerview.visibility = View.GONE
        progress_bar.visibility = View.GONE
        textview.visibility = View.VISIBLE
        textview.text = "Something went wrong"
    }

}
