package com.aricent.vvc

import android.app.Application

open class TestUrlApplication : Application() {
  open fun getBaseUrl() = "https://www.xyz.com"
}