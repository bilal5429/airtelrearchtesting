/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aricent.view

import android.os.Bundle
import androidx.lifecycle.Observer
import com.aricent.model.InternetModel
import com.aricent.viewmodel.InternetStatusViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Base Activity for other activities, handle network on off.
 */
abstract class BaseActivity : DaggerAppCompatActivity() {
    @set:Inject
    var internetViewModel: InternetStatusViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        internetViewModel!!.internetUtil!!.observe(this,
                Observer<InternetModel?> { internetModel ->
                    if (!internetModel!!.isConnected) {
                        onNetworkDisconnected()
                    } else {
                        onNetworkConnected()
                    }
                })
    }

    /**
     * callback, When internet connected
     */
    abstract fun onNetworkConnected()

    /**
     * callback, When internet disconnected
     */
    abstract fun onNetworkDisconnected()

}
