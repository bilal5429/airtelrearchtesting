/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aricent.view

import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.aricent.utils.Constants.Companion.APP_RESTRICT_DIALOG_TAG
import com.aricent.utils.Constants.Companion.PERMISSION_LIST
import com.aricent.utils.Constants.Companion.PERMISSION_REQUEST_CODE
import com.aricent.view.fragment.AppRestrictDialogFragment
import com.aricent.viewmodel.PermissionViewModel
import com.aricent.vvc.R
import javax.inject.Inject

/**
 * Splash activity used for handle permission and init JNI stack.
 * If permission is denied by user, App Block dialog is shown to user.
 */

class SplashActivity : BaseActivity(), AppRestrictDialogFragment.ItemClickCallBack,
        ActivityCompat.OnRequestPermissionsResultCallback {

    @set:Inject
    var mPermissionViewModel: PermissionViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)
        mPermissionViewModel!!.permissionUtil!!.observe(this, Observer { permissionModel ->
            if (!permissionModel!!.isPermissionGranted) {
                requestPermissions()
            } else {
                //open Activity
            }
        })
    }

    override fun onNetworkConnected() {
        Toast.makeText(this, "net found", Toast.LENGTH_LONG).show()
    }

    override fun onNetworkDisconnected() {
        Toast.makeText(this, "net not found", Toast.LENGTH_LONG).show()
    }

    override fun onProceedClick() {
        requestPermissions()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "permission Granted", Toast.LENGTH_LONG).show()
            } else {
                AppRestrictDialogFragment().show(supportFragmentManager, APP_RESTRICT_DIALOG_TAG)
            }

        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSION_LIST, PERMISSION_REQUEST_CODE)
    }
}