/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aricent.component

import com.aricent.application.MainApplication
import com.aricent.module.ActivityBuilderModule
import com.aricent.module.AppModule
import com.aricent.module.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


/**
 * Base component to handle context and modules for dependency injection.
 */
@Singleton
@Component(modules = [AppModule::class, AndroidInjectionModule::class,
    ViewModelFactoryModule::class, ActivityBuilderModule::class])

/**
 * interface for App component
 */
interface AppComponent : AndroidInjector<MainApplication?> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MainApplication?): Builder?
        fun context(context: AppModule?): Builder?
        fun build(): AppComponent?
    }
}