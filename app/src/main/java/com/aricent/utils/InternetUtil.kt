/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aricent.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.lifecycle.LiveData
import com.aricent.model.InternetModel
import javax.inject.Inject

/**
 * Class is used to detect internet detection.
 */
class InternetUtil @Inject constructor(private var mContext: Context?) :
        LiveData<InternetModel?>() {

    override fun onActive() {
        super.onActive()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        mContext!!.registerReceiver(internetReceiver, filter)
    }

    override fun onInactive() {
        super.onInactive()
        mContext!!.unregisterReceiver(internetReceiver)
    }

    private val internetReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.extras != null) {
                val activeNetwork = intent.extras[ConnectivityManager.EXTRA_NETWORK_INFO] as NetworkInfo
                val isConnected = activeNetwork != null && activeNetwork.isConnected
                value = InternetModel(isConnected)
            }
        }
    }

}