/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aricent.utils

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import com.aricent.model.PermissionModel
import javax.inject.Inject

class PermissionUtil @Inject constructor(private var mContext: Context?) :
        LiveData<PermissionModel>() {

    override fun onActive() {
        super.onActive()
        checkPermissionGranted()
    }

    private fun checkPermissionGranted() {
        var permissionGrantedCount = 0
        val permissionList = Constants.PERMISSION_LIST
        for (permission in permissionList) {
            if (ActivityCompat.checkSelfPermission(mContext!!, permission) ==
                    PackageManager.PERMISSION_GRANTED) {
                permissionGrantedCount++
            }
        }
        value = PermissionModel(permissionGrantedCount == permissionList.size)
    }

}