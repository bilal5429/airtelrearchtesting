/*
 * Software Name : VVC App
 *
 * Copyright (c) 2020. Altran Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aricent.application

import com.aricent.component.DaggerAppComponent
import com.aricent.module.AppModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


/**
 * Base class for maintaining global application state and extends DaggerApplication
 *
 */
class MainApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication?>? {
        return DaggerAppComponent.builder().application(this)!!.
        context(AppModule(this))!!.build()
    }
}